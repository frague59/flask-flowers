'use strict';
// import axios from "./node_modules/axios/dist/axios.min.js";
import './lorem-lipsum.js'
import {FlowersCanvas, getRandomInt} from './modules/canvas.js'

const choose = (choices) => {
  let index = Math.floor(Math.random() * choices.length);
  return choices[index];
}

const getRandomTexts = (count) => {

  const lorem = new LoremIpsum()
  let output = []
  for (let i = 0; i < count; i++) {
    output.push(lorem.sentence(2, 6))
  }
  return output
}
// const apiUrl = "/api/"

const initCanvas = (el, width, height, colors, fonts) => {
  document.addEventListener("DOMContentLoaded", evt => {
    // window.setTimeout(() => {
    const flowers_canvas = new FlowersCanvas(el, width, height);
    const texts = getRandomTexts(150)
    for (let text of texts) {
      const color = choose(colors)
      const font = choose(fonts)
      const size = getRandomInt(10, 40)
      const step = Math.random() / 3.5
      const left_top = flowers_canvas.writeTextAsTree(text, color, font, size, step)
      flowers_canvas.addLeaves(left_top[0], left_top[1])
    }
    // }, 1000)
  })
}

export default initCanvas
