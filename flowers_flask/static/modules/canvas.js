const DEBUG = true
const DEFAULT_STEP = 0.1
const DEFAULT_FONT = "Arial"
const DEFAULT_SIZE = 40
const MARGIN = 4
const TEXT_TIMEOUT = 500
const CHARS_TIMEOUT = 200

const createCanvas = (el, width, height) => {
  const parentElement = document.querySelector(el)
  if (parentElement == null) {
    console.error(`ERROR::canvas::Unable to locate element ${el}`)
    return null
  }

  const canvas = document.createElement('canvas')
  canvas['width'] = width
  canvas['height'] = height
  canvas['id'] = 'canvas'
  canvas["style"] = "background: #FFFFFF"
  const ctx = canvas.getContext("2d")

  parentElement.appendChild(canvas)
  DEBUG && console.debug(`DEBUG::canvas:: initialized:`, canvas)
  return canvas
}

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const revertString = (text) => {
  const chars = []
  for (let char of text) {
    chars.push(char)
  }
  const reverted = chars.reverse()
  DEBUG && console.debug(`DEBUG::revertString() color:`, reverted)
  return reverted
}

const Oscillator = class Oscillator {
  _freq = 0
  _amplitude = 0

  constructor(freq, amplitude) {
    this._freq = freq
    this._amplitude = amplitude
  }

  get freq() {
    return this._freq
  }

  get amplitude() {
    return this._amplitude
  }
}

const FlowerTree = class FlowerTree {
  canvas = null //
  text = null
  color = null
  font = null

  constructor(canvas, text, color, font) {
    this.canvas = canvas
    this.text = text
    this.color = color
    this.font = font
  }

  async writeChar(char, x, y, color, size) {
    if (! this.canvas?.getContext) {
      console.error("ERROR::FlowerTree::writeChar() Unable to get the canvas context !")
      return
    }
    const ctx = this.canvas.getContext('2d')
    ctx.font = `${size}px ${this.font}`
    ctx.fillStyle = color
    ctx.textAlign = "center"
    ctx.fillText(char, x, y)
  }

  async addTextAsTrunk(text, size, step, oscillator) {
    const reverted = revertString(text)
    let _pos_x = getRandomInt(1, this.canvas.width)
    let _pos_y = this.canvas.height - MARGIN
    let _size = size
    DEBUG && console.debug(`DEBUG::FlowerTree::addTextAsTrunk() this._pos_x:${_pos_x} this._pos_y:${_pos_y}`)
    for (let char of reverted) {
      await this.writeChar(char, _pos_x, _pos_y, this.color, _size, this.font)
      _size = Math.round(_size / (1 + step))
      _pos_y -= _size
      _pos_x = Math.round(Math.sin(_pos_y / oscillator.freq) * oscillator.amplitude / (1 + step)) + _pos_x
      DEBUG && console.debug(`DEBUG::FlowerTree::addTextAsTrunk() *RUN* (${char}) x:${_pos_x} y:${_pos_y} size:${_size}`)
      if (_pos_y <= 0) {
        break
      }
    }
    return [_pos_x, _pos_y]
  }

  async addLeaves(x, y) {

  }
}

const FlowersCanvas = class FlowersCanvas {
  el = 0
  width = 0
  height = 0
  _canvas = null

  constructor(el, width, height) {
    this.el = el
    this.width = width
    this.height = height
  }

  get canvas() {
    if (this._canvas != null) {
      return this._canvas
    }
    this._canvas = createCanvas(this.el, this.width, this.height)
    return this._canvas
  }

  async writeTextAsTree(text, color, font, size, step, oscillator) {
    if (font === undefined) {font = DEFAULT_FONT}
    if (size === undefined) {font = DEFAULT_SIZE}
    if (step === undefined) {step = DEFAULT_STEP}
    if (oscillator === undefined) {oscillator = new Oscillator(getRandomInt(30, 150), getRandomInt(0, 20))}
    let tree = new FlowerTree(this.canvas, text, color, font, step)
    await tree.addTextAsTrunk(text, size, step, oscillator)
  }

}

// Exports
export {FlowersCanvas, getRandomInt}
