"""
DB Settings
"""
import logging

from wtfpeewee.orm import model_form

from . import db

logger = logging.getLogger(__name__)
__author__ = "fguerin"

TextRequestForm = model_form(db.TextRequest)
