"""
Simple Flask application to draw text flowers on a JS canvas

:author: François GUÉRIN <fguerin@ville-tourcoing.fr>

"""
import logging
import os
from typing import Optional, Tuple

from flask import Flask, render_template, request
from flask_bootstrap import Bootstrap
from peewee import Database

from . import forms

__version__ = "0.0.1"
logger = logging.getLogger(__name__)


def create_app(test_config=None) -> Tuple[Flask, Database]:
    # create and configure the app
    _app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        _app.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        _app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(_app.instance_path)
    except OSError:
        pass

    bootstrap = Bootstrap(_app)  # noqa
    _database = Database(_app)
    return _app, _database


app, database = create_app()


# @app.before_request
# def _db_connect():
#     database.connect()
#
#
# @app.teardown_request
# def _db_close(exc: Any):
#     if not database.is_closed():
#         database.close()


@app.context_processor
def inject_css_classes():
    return {
        "header_css": "container",
        "main_css": "container",
        "footer_css": "container fixed",
    }


@app.route("/", methods=["GET", "POST"])
def get_form():
    if request.method == "GET":
        return render_template("form.html", form=forms.TextRequestForm())
    else:
        text_request = forms.TextRequestForm(request.form)
        text_request.save()


@app.route("/api/")
def get_api():
    return {}


@app.route("/display/")
@app.route("/display/<theme>/")
def get_display(theme: Optional[str] = None):
    theme = theme if theme is not None else "DEFAULT"
    colors = app.config.get("FLOWERS_DISPLAY", {}).get(theme, {}).get("COLORS", [])
    fonts = app.config.get("FLOWERS_DISPLAY", {}).get(theme, {}).get("FONTS", [])
    width = 1280
    height = 768

    logger.debug("get_display()")
    return render_template("display.html", apiUrl="/api/", colors=colors, fonts=fonts, width=width, height=height,)
