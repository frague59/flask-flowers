"""
DB Settings
"""
import logging

import peewee

logger = logging.getLogger(__name__)

database = peewee.SqliteDatabase("requests.db")


class TextRequest(peewee.Model):
    text = peewee.CharField(max_length=255)
    color = peewee.CharField(max_length=255, default="#003366")
